Yahoo! Browser-Based Authentication for Drupal

Author: Jason Levitt
Date: September 18th, 2006
Version: 1.0

Requirements
------------
* Drupal 4.7.x
* PHP4 or PHP5 
* PHP Curl extension

Overview
---------

This first version of Yahoo_bbauth for Drupal lets new users login to your Drupal site using their Yahoo login credentials. Drupal accounts are automatically created using the username 
[userhash]@yahoo . The userhash is a unique Yahoo identifier that is tied to your Yahoo! user id. 

This module makes use of Yahoo's Browser Based Authentication -- a free service. For details on Browser Based Authentication, see: http://developer.yahoo.com/auth

Install
-------

1. Upload the yahoo_bbauth folder to the Drupal modules/ directory.

2. Activate the module via the Drupal module configuration menu:
   "administer >> settings >> modules"

3. Set the Appid and Secret in your settings: "administer >> settings >> yahoo_bbauth"
   See "Getting a Yahoo! Application ID and Secret" below.

4. In "administer >> settings >> users" make sure this is checked: 
   "Visitors can create accounts and no administrator approval is required."

5. In "administer >> blocks", create a "yahoo_bbauth login" block.

6. Click on the Login link on your home page and login using a Yahoo userid and password.

Getting a Yahoo! Appid and Secret
------------------------------------------

Get your application ID and secret by registering here (it's free!):

https://developer.yahoo.com/wsregapp/index.php

When you fill out the registration form, if your site uses
"clean URLs", then make your "Web Application URL"

[path to your Drupal site]/yahoo_bbauth_login

Otherwise, the "Web Application URL" should be

[path to your Drupal site]/?q=yahoo_bbauth_login

So, if your Drupal site is located at http://yourdomain.com/somedir, then
you put either http://yourdomain.com/somedir/yahoo_bbauth_login as your Web Application URL, or http://yourdomain.com/somedir/?q=yahoo_bbauth_login, depending on whether
your site uses "clean URLs" or not.

Also, select the radio button labeled: "I want to use Browser Based Authentication for user authentication but I don't need to access a specific Yahoo! property"

Note
-----
The userhash is unique for each Yahoo! user id, but the userhash is tied to the Application ID and Secret. If these values change, then the userhash value will change. If you change the path to your Drupal site (example:  http://www.mysite.com/mysite  changed  to  http://www.mysite.com/mysite/new), then you will need to register for a new Application ID and Secret using that path. The computed userhash values, while still unique, will be different. Since the usernames are [userhash]@yahoo, they will require migration to the new userhash values. 

Another Note
------------
If your site has caching enabled, you should set it to cache the front page for
10 minutes or less. Otherwise, the authorization link will time out and
be unuseable until the cache refreshes.

Patch
-----
In Drupal 4.73 (and probably 4.70-4.72), this patch fixes irritating user interface bugs in Drupal's remote authentication dialogs:  http://drupal.org/node/80819
Not mandatory, but probably a good idea to apply anyway.

